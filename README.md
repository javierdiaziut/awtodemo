# AwtoDemo

Prueba técnica para Awto

## Getting started

Clonar el proyecto y hacer checkout a la rama de develop.

En este proyecto se implementó clean architecture como arquitectura base. En el Splash Screen se implementó una animación lottie para mejorar el look and feel del splash, también se usaron algunas técnicas de clean code.  

El proyecto básicamente consulta la API pública "https://v2.jokeapi.dev/joke/Any" con el fin de obtener chiste en diferentes formatos y mostrarlos al usuario de la manera correcta. 


## Componentes utilizados:

- Corrutinas
- Lottie animación
- Constraint layout
- Retrofit
- Clean code
- ViewModels
- LiveData
- ViewBinding

